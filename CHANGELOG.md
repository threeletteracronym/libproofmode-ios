# LibProofMode

## 0.1.9

- Make `LocationManager` thread safe.

## 0.1.8

- Make the "proofFilePath" public and accept an optional "fileSuffix" parameter.

## 0.1.7

- Added the `Proof.shared.synchronousNotarization` flag (default true) that controls whether to wait for notarization to complete before proof generation returns.

## 0.1.6

- Added `Proof.shared.defaultDocumentFolder` for setting default documents folder for proof files. Useful when e.g. using app groups.
- Always include a value for `deviceId` in the proof data, setting it to "" for cases where we don't have permissionn to read it.

## 0.1.5

- When fetching assets, set 'isNetworkAccessAllowed' to true, to be able to fetch items that are moved to iCloud etc.
- Make "withData" open, so can be used by subclasses.

## 0.1.1

- :warning: BREAKING CHANGE! - Remove the `data` parameter from `Proof.shared.getProof()`. We already have a data property on the mediaItem, so use that.
- Added `MediaItem.getProofData()` to get existing proof data, if any.
