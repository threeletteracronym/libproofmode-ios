//
//  ExampleApp.swift
//  Example
//
//  Created by N-Pex on 2023-02-02.
//

import SwiftUI

@main
struct ExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
